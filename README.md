# Resources for swagger workshops
## Useful links:
* https://hub.docker.com/u/swaggerapi
* https://swagger.io/blog/news/whats-new-in-openapi-3-0
* https://swagger.io/blog/api-strategy/difference-between-swagger-and-openapi/
* https://github.com/springdoc/springdoc-openapi
* https://github.com/kongchen/swagger-maven-plugin
## Useful commands:
* docker-compose up
* docker run -p 80:8080 swaggerapi/swagger-editor
* docker run -p 80:8080 swaggerapi/swagger-ui
## Useful local links:
* http://localhost:8080/v3/api-docs
* http://localhost:8080/v3/api-docs.yaml
* 
