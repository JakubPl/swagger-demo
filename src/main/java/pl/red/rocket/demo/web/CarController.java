package pl.red.rocket.demo.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.red.rocket.demo.model.Car;

import java.math.BigDecimal;
import java.util.Optional;

@RestController
@RequestMapping("car")
public class CarController {
    //test with response entity
    @GetMapping("response-entity")
    public ResponseEntity<Car> getCarByBrand(String brand) {
        Car car = new Car();
        car.setBrand(brand);
        return ResponseEntity.ok(car);
    }

    //simple test
    @GetMapping("simple")
    public Car getCarByName(String model) {
        Car car = new Car();
        car.setModel(model);
        return car;
    }

    //path variable test
    @GetMapping("path-variable/{price}")
    public Car getCarByName(@PathVariable("price") BigDecimal price) {
        Car car = new Car();
        car.setPrice(price);
        return car;
    }

    //optional test
    @GetMapping("optional")
    public Optional<Car> getCarByHorsePower(@RequestParam("horsePower") int horsePower) {
        Car car = new Car();
        car.setHorsePower(horsePower);
        return Optional.of(car);
    }

    //custom generic test
    @GetMapping("custom-generic")
    public Response<Car> getCarByName() {
        return new Response<>(new Car());
    }
}
