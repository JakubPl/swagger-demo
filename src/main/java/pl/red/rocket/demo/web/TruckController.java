package pl.red.rocket.demo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.red.rocket.demo.model.Car;
import pl.red.rocket.demo.model.Truck;

@RestController
@RequestMapping("house")
public class TruckController {
    //test with response entity
    @GetMapping("inheritance")
    public Truck getTruckInheritance1() {
        return new Truck();
    }

    //it just looks at the method signature
    @GetMapping("inheritance2")
    public Car getTruckInheritance2() {
        return new Truck();
    }

    @GetMapping("generics2")
    public Response<Truck> getTruckGenerics2() {
        return new Response<>(new Truck());
    }
}
