package pl.red.rocket.demo.web;

public class Response<T> {
    T value;

    public Response() {
    }

    public Response(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
